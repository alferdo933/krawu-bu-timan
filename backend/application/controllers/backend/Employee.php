<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('auth');
        $this->load->model(array('m_employee'));
        
        $this->auth->authenticate(); //authenticate logged in user

        $this->auth->set_module('employee'); //check for current module
        
        $this->auth->authorize();    //authorize the user        
	}
	
	public function index()
	{
        $this->auth->set_access_read();

		$data = array();

        $data['page_title'] = 'Pegawai Restoran';
        $data['plugin'] = array();
        $data['custom_js'] = array(
            'data' => $data,
            'src'  => 'backend/__scripts/employee'
        );
        $data['assets_js'] = array();
        $data['role'] = $this->auth->all_permission();

        //$data['data_group'] = $this->m_employeegroup->get();

        $this->load->view('backend/__base/header_dashboard',$data);
        $this->load->view('backend/__base/sidebar',$data);
        $this->load->view('backend/employee/index',$data);
        $this->load->view('backend/__base/footer_dashboard',$data);
    }

    public function get_datatable(){
        //allow ajax only
        if(!$this->input->is_ajax_request()) show_404();

        $this->auth->set_access_read();

        $filter_cols = array("em_status != ".DELETED);

        $this->m_employee->get_datatable(implode(" AND ",$filter_cols),"em_name desc");
    }

    public function detail(){
        if(!$this->input->is_ajax_request()) show_404();

        $this->auth->set_access_read();

        $pk_id = $this->input->post('em_id');
        if($pk_id === NULL)
            json_response('error');

        $pk_id = intval($pk_id);

        $this->m_employee->set_select_mode('detail');
        $detail = $this->m_employee->get_by_multiple_column(array(
                                                                    'em_id' => $pk_id,
                                                                    'em_status !=' => DELETED
                                                                ));
                                                            
        //output
        if($detail !== NULl) json_response('success','',$detail);
        else json_response('error','Data tidak ada');
    }

    public function add(){

		$this->auth->set_access_create();

		$this->save();
    }
    
    public function edit(){
        $this->auth->set_access_update();

        $this->save();
    }

    private function save(){
        if(!$this->input->is_ajax_request()) show_404();

        //validation
		$this->load->library('form_validation');
        $this->form_validation->set_rules('em_id'    , 'Id'            , 'integer');
        $this->form_validation->set_rules('em_name'  , 'name Staff'  , 'trim|required|max_length[255]');
        $this->form_validation->set_rules('em_birthday'  , 'Birthday Staff'  , 'required');
        $this->form_validation->set_rules('em_gender'  , 'gender Staff'         , 'integer');
        $this->form_validation->set_rules('em_status'  , 'status Staff'  , 'integer');
        $this->form_validation->set_rules('em_jabatan'  , 'jabatan Staff'  , 'integer');
        $this->form_validation->set_rules('em_jamkerja'  , 'jamkerja Staff'  , 'integer');
        $this->form_validation->set_rules('em_diterima'  , 'diterima Staff'  , 'required');

        if($this->form_validation->run()){
            $current_time = date('Y-m-d H:i:s');
            $birth_date = $this->input->get('birth_date');
            $diterima = $this->input->get('diterima');
            //insert
            if($this->input->post('em_id') == ''){
                $em_id = $this->m_employee->insert($this->input->post('em_name'),
                                                $birth_date,
                                                $this->input->post('em_gender'),
                                                $this->input->post('em_status') == ACTIVE ? ACTIVE : SUSPEND,
                                                $this->input->post('em_jabatan'),
                                                $this->input->post('em_jamkerja'),
                                                $diterima
                                                );
                json_response('success','Sukses menambah');
            }
            //update
            else{
                $pk_id = $this->input->post('em_id');
                //make integer
                $pk_id = intval($pk_id);
                $edited = $this->m_employee->get_by_multiple_column(array(
                                                'em_id' => $pk_id,
                                                'em_status !=' => DELETED
                ));
                if($edited !== NULL){
                    $this->m_employee->update($pk_id,
                                                $this->input->post('em_name'),
                                                $birth_date,
                                                $this->input->post('em_gender'),
                                                $this->input->post('em_status') == ACTIVE ? ACTIVE : SUSPEND,
                                                $this->input->post('em_jabatan'),
                                                $this->input->post('em_jamkerja'),
                                                $diterima
                                                );
                    json_response('success','Sukses Edit');
                }else
                    json_response('error','Data tidak ditemukan');
            }
        }else
            json_response('error',validation_errors());
    }

    public function delete(){
        if(!$this->input->is_ajax_request()) show_404();

        $this->auth->set_access_delete();
        if($this->input->post('em_id') === NULL) json_response('error');
        
        $all_deleted = array();
		foreach($this->input->post('em_id') as $row){
			$row = intval($row);
            $deleted = $this->m_employee->get_by_column($row);
			if($deleted !== NULL){
                $this->m_employee->update_single_column('em_status', DELETED, $row);
                $all_deleted[] = array("id" => $row, "status" => "success");
			}
        }
        if(count($all_deleted) > 0){
            json_response('success','sukses hapus');
        }else{
            json_response('error','gagal hapus');
        }

    }


    
}