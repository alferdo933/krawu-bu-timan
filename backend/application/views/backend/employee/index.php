<div class="main-content">
    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="fas fa-fw fa-utensils bg-blue"></i>
                        <div class="d-inline">
                            <h5><?php echo $page_title;?></h5>
                            <span>Modul untuk konfigurasi Pegawai Restoran</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <nav class="breadcrumb-container" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="<?php echo base_url();?>backend/dashboard"><i class="ik ik-home"></i></a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page"><?php echo $page_title;?>
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="slide-content col-md-12" id="table_wrapper">
                <div class="card">
                    <div class="card-header"><h3><?php echo $page_title;?></h3></div>
                    <div class="card-body">
                        <div class="float-sm-right">
                            <a class="btn btn-danger btn-sm delete_role" id="multidel_act" >
                                <i class="fas fa-trash"></i> Hapus
                            </a>
                            <a class="btn btn-success btn-sm add_role" id="add_act" >
                                <i class="fas fa-plus"></i> Tambah
                            </a>
                        </div>
                        <br>
                        <br>
                        <div class="table-responsive">
                        <table id="employee_table" class="table ">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Nama</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Status</th>
                                    <th>Jabatan</th>
                                    <th>Jam Kerja</th>
                                    <th>Diterima</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4" id="detail_wrapper" style="display: none;">
                <div class="card">
                    <div class="card-header" id="detail_title"><h3></h3></div>
                    <div class="card-body">
                    <form id="form_detail" data-mode="">
                        <input type="hidden" id="em_id" name="em_id"/>
                        <div class="form-group">
                            <label for="em_name">Nama</label>
                            <input type="text" id="em_name" name="em_name" class="form-control" placeholder="Masukkan nama ">
                            <small class="form-text text-muted"></small>
                        </div>
                        <div class="form-group col-md-4 mb-0">
                            <label for="em_birthday">Tanggal Lahir</label><br>
                            <div class="input-group date " id="em_birthday" data-target-input="nearest">
                                <input type="text" value="<?php echo $birth_date;?>" class="form-control datetimepicker-input" name="em_birthday" data-target="#birth_date"/>
                                <div class="input-group-append" data-target="#birth_date" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                          <label for="em_gender">Jenis Kelamin</label>
                          <br>
                          <input type="checkbox" id="em_gender" class="switch-slider" name="em_gender" checked value="1" data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-on="Pria" data-off="Wanita" data-width="100">
                        </div>
                        <div class="form-group">
                          <label for="em_status">Status</label>
                          <br>
                          <input type="checkbox" id="em_status" class="switch-slider" name="em_status" checked value="1" data-toggle="toggle" data-onstyle="success" data-offstyle="danger" data-on="Aktif" data-off="Suspend" data-width="90">
                        </div>  
                        <div class="form-group">
                          <label for="em_jabatan">Jabatan</label>
                          <select class="form-control" name="em_jabatan" id="em_jabatan">
                            <option value="">Silahkan pilih</option>
                            <?php foreach($data_group as $row){ ?>
                                <option value="<?php echo $row->jb_id; ?>"><?php echo $row->jb_name; ?></option>
                              <?php } ?>
                          </select>
                          <small class="form-text text-muted"></small>
                        </div>
                        <div class="form-group">
                          <label for="em_jamkerja">Jam Kerja</label>
                          <select class="form-control" name="em_jamkerja" id="em_jamkerja">
                            <option value="">Silahkan pilih</option>
                            <?php foreach($data_group as $row){ ?>
                                <option value="<?php echo $row->jk_id; ?>"><?php echo $row->jk_name; ?></option>
                              <?php } ?>
                          </select>
                          <small class="form-text text-muted"></small>
                        </div>
                        <div class="form-group col-md-4 mb-0">
                            <label for="em_diterima">Diterima</label><br>
                            <div class="input-group date " id="em_diterima" data-target-input="nearest">
                                <input type="text" value="<?php echo $diterima;?>" class="form-control datetimepicker-input" name="em_diterima" data-target="#diterima"/>
                                <div class="input-group-append" data-target="#diterima" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                          <button type="submit" class="btn btn-primary float-right">Submit</button>
                          <button type="button" class="btn btn-light float-right mr-2" onclick="hide_detail()">Close</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>