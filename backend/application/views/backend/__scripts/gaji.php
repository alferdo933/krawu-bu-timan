<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
$(document).ready(function() {

    var url = {
        data : base_url+'backend/gaji/get_datatable',
        detail : base_url+'backend/gaji/detail'
    };

    var dataTableObj = $("#gaji_table").DataTable({
        processing:true,
        serverSide:false,
        bProcessing: true,
        stateSave: false,
        pagingType: 'full_numbers',
        ajax:{
            url: url.data,
            type: 'POST',
            dataType: "json",
            data: (dataReq)=>{ dataReq[csrf_name] = Cookies.get(csrf_name);return dataReq;}, 
        },
        'columns':[ 
            { data:"gj_employee", visible: true},
            { data:"gj_total", 
                render:function(data,type,meta){
                    return 'Rp. '+toCurrency(data);
                } 
            },
            { data:"gj_tanggal_awal" , visible: true},
            { data:"gj_tanggal_akhir" , visible: true}
        ],
        'createdRow': function(row,data,dataIndex){
          $(row).addClass('rowData').attr('data-id',data.gj_employee);
        }
    });

    init_role(<?php echo json_encode($role);?>);

    //handler submit form
    submitForm_handler('#form_detail',url,dataTableObj);
});
</script>