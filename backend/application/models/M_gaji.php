<?php defined('BASEPATH') or exit('No direct script access allowed');

class M_gaji extends MY_Model
{
    protected $pk_col = 'gj_employee';
    protected $table_name = 'gaji';

    public function __construct()
    {
        parent::__construct();
    }

    public function select()
    {
        if ($this->default_select) {
            $this->db->select('em_name AS gj_employee');
            $this->db->select('gj_total');
            $this->db->select('gj_tanggal_awal');
            $this->db->select('gj_tanggal_akhir');
        } else {
            //for detail get like sensitive information
            $this->db->select('em_id AS gj_employee');
            $this->db->select('gj_total');
            $this->db->select('gj_tanggal_awal');
            $this->db->select('gj_tanggal_akhir');
        }
        $this->db->from($this->table_name);
        $this->db->join('employee', 'em_id = gj_employee');
    }

    public function insert(
                            $gj_tanggal_awal = FALSE,
                            $gj_tanggal_akhir = FALSE
    ) {
        $data = array();
        if ($gj_tanggal_awal   !== FALSE) $data['gj_tanggal_awal'] = trim($gj_tanggal_awal);
        if ($gj_tanggal_akhir   !== FALSE) $data['gj_tanggal_akhir'] = trim($gj_tanggal_akhir);

        $this->db->insert($this->table_name, $data);
        return $this->db->insert_id();
    }

    public function update(
                            $gj_employee = FALSE,
                            $gj_tanggal_awal = FALSE,
                            $gj_tanggal_akhir = FALSE
    ) {
        $data = array();
        if ($gj_tanggal_awal   !== FALSE) $data['gj_tanggal_awal'] = trim($gj_tanggal_awal);
        if ($gj_tanggal_akhir   !== FALSE) $data['gj_tanggal_akhir'] = trim($gj_tanggal_akhir);

        return $this->db->update($this->table_name, $data, 'gj_employee = ' . $gj_employee);
    }
}
