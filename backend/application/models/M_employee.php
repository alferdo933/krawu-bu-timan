<?php defined('BASEPATH') or exit('No direct script access allowed');

class M_employee extends MY_Model
{
    protected $pk_col = 'em_id';
    protected $table_name = 'employee';

    public function __construct()
    {
        parent::__construct();
    }

    public function select()
    {
        if ($this->default_select) {
            $this->db->select('em_id');
            $this->db->select('em_name');
            $this->db->select('em_birthday');
            $this->db->select('em_gender');
            $this->db->select('em_status');
            $this->db->select('jb_jabatan AS em_jabatan');
            $this->db->select('jk_name AS em_jamkerja');
            $this->db->select('em_diterima');
        } else {
            //for detail get like sensitive information
            $this->db->select('em_id');
            $this->db->select('em_name');
            $this->db->select('em_birthday');
            $this->db->select('em_gender');
            $this->db->select('em_status');
            $this->db->select('jb_id AS em_jabatan');
            $this->db->select('jk_id AS em_jamkerja');
            $this->db->select('em_diterima');
        }
        $this->db->from($this->table_name);
        $this->db->join('jabatan', 'jb_id = em_jabatan');
        $this->db->join('jamkerja', 'jk_id = em_jamkerja');
    }

    public function insert(
                            $em_name = FALSE,
                            $em_birthday = FALSE,
                            $em_gender = FALSE,
                            $em_status = FALSE,
                            $em_jabatan = FALSE,
                            $em_jamkerja = FALSE,
                            $em_diterima = FALSE
    ) {
        $data = array();
        if ($em_name   !== FALSE) $data['em_name'] = trim($em_name);
        if ($em_birthday   !== FALSE) $data['em_birthday'] = trim($em_birthday);
        if ($em_gender  !== FALSE) $data['em_gender'] = intval($em_gender);
        if ($em_status  !== FALSE) $data['em_status'] = intval($em_status);
        if ($em_jabatan !== FALSE) $data['em_jabatan'] = intval($em_jabatan);
        if ($em_jamkerja   !== FALSE) $data['em_jamkerja'] = trim($em_jamkerja);
        if ($em_diterima   !== FALSE) $data['em_diterima'] = trim($em_diterima);

        $this->db->insert($this->table_name, $data);
        return $this->db->insert_id();
    }

    public function update(
                            $em_id = FALSE,
                            $em_name = FALSE,
                            $em_description = FALSE,
                            $em_value = FALSE,
                            $em_unit = FALSE,
                            $em_status = FALSE,
                            $em_created = FALSE,
                            $em_updated = FALSE
    ) {
        $data = array();
        if ($em_name   !== FALSE) $data['em_name'] = trim($em_name);
        if ($em_birthday   !== FALSE) $data['em_birthday'] = trim($em_birthday);
        if ($em_gender  !== FALSE) $data['em_gender'] = intval($em_gender);
        if ($em_status  !== FALSE) $data['em_status'] = intval($em_status);
        if ($em_jabatan !== FALSE) $data['em_jabatan'] = intval($em_jabatan);
        if ($em_jamkerja   !== FALSE) $data['em_jamkerja'] = trim($em_jamkerja);
        if ($em_diterima   !== FALSE) $data['em_diterima'] = trim($em_diterima);

        return $this->db->update($this->table_name, $data, 'em_id = ' . $em_id);
    }
}
