            <!-- masterslider -->
            <div class="master-slider ms-skin-black-2 round-skin" id="masterslider">
				<!-- new slide -->
				<div class="ms-slide">
					<!-- slide background -->
					<img src="js/masterslider/blank.gif" data-src="http://4.bp.blogspot.com/-XlF5pGN8oOo/ThP9qbmfHII/AAAAAAAAB30/qzixswnA4JQ/s1600/DSC_1607.JPG" alt="">
					<!-- slide text layer -->
					<div class="ms-layer ms-caption" style="color: #DAF7A6;">
						<h1 class="text-left">
							<span>Selamat Datang</span>
							<br>Nasi Krawu Bu Timan
						</h1>
						<a href="<?php echo base_url();?>Menu" class="btn btn-default"><i class="fa fa-file-text-o"></i>Order</a>
					</div>

				</div>
				<!-- end of slide -->
				<!-- new slide -->
				<div class="ms-slide">
					<!-- slide background -->
					<img src="js/masterslider/blank.gif" data-src="https://media.travelingyuk.com/wp-content/uploads/2017/04/Nasi-Krawu-Bu-Timan-Gresik.jpg" alt="">
					<!-- slide text layer -->
					<div class="ms-layer ms-caption" style="">
						<h1 class="text-right">
						</h1>
						<a href="index.html#" class="btn btn-default"><i class="fa fa-file-text-o"></i>Read  More</a>
					</div>
				</div>

				<!-- new slide -->
				<div class="ms-slide">
					<!-- slide background -->
					<img src="js/masterslider/blank.gif" data-src="https://cicaakcerdas.files.wordpress.com/2014/01/dscn0297.jpg" alt="">
					<!-- slide text layer -->
					<div class="ms-layer ms-caption" style="">
						<h1>
						<a href="index.html#" class="btn btn-default"><i class="fa fa-file-text-o"></i> Read  More</a>
					</div>
				</div>
				<!-- end of slide -->
			</div>
			<!-- end of masterslider -->

			<!-- purchase TakeAway section start -->
			<div class="container">
				<div class="call-to-action-section">
					<div class="css-table-cell">
						<div class="icon">
							<img src="assets/rapidrms/img/logo-light-krawu.png" alt="">
						</div>
					</div>
					<div class="text css-table">
						<div class="css-table-cell">
							<h4><?php echo SITE_NAME;?></h4>
							<p>Restoran ini bertempat di Jl Wahidin/ Raya Bunder, Kebomas, Gresik, anda dapat membeli Nasi Krawu Khas Kabupaten Gresik secara online melalui website ini.</p>
						</div>
                        <!--
						<div class="css-table-cell">
							<a href="index.html#" class="btn btn-default-red  pad-bottom"><i class="fa fa-file-text-o"></i> Read  More</a>
						</div>-->

						<div class="css-table-cell">
							<a href="<?php echo base_url();?>menu" class="btn btn-default-red-inverse pad-top"><i class="fa fa-shopping-cart"></i> Pesan Makan</a>
						</div>
					</div>
				</div>
				<!-- end .call-to-action-section -->
			</div>
			<!-- end .container -->
			<!-- end purchase TakeAway section  -->

			<!-- start .category box section -->
			<div class="category-boxes-icons">
				<div class=" container">
					<div class="row">
						<div class="col-md-3 col-sm-6 col-xs-12 text-center">
							<div class="category-boxes-item">
								<figure>
									<img class="img-responsive" src="https://kedaikayumanis.com/v2019/wp-content/uploads/2019/01/meja-kedai-kayumanis-interior.jpg">
									<h4>Reservasi Meja</h4>
									<figcaption> <a href="<?php echo base_url();?>reservation" class="btn btn-default-white"><i class="fa fa-shopping-cart"></i> Pesan Sekarang</a> 
									</figcaption>
								</figure>
							</div>
						</div>
						<div class="col-md-3 col-sm-6 col-xs-12  text-center">
							<div class="category-boxes-item">
								<figure>
									<img src="https://cdn1-production-images-kly.akamaized.net/HcUxLIagjCys1EIL2CRWVqvmHAQ=/1360x766/smart/filters:quality(75):strip_icc():format(jpeg)/kly-media-production/medias/2485146/original/048806000_1543326999-resep-idul-adha-tumis-daging-sapi-saus-tiram.jpg">
									<h4>Daging</h4>
									<figcaption><a href="<?php echo base_url();?>menu" class="btn btn-default-white"><i class="fa fa-shopping-cart"></i> Pesan Sekarang</a>
									</figcaption>
								</figure>
							</div>
						</div>

						<div class="col-md-3 col-sm-6 col-xs-12  text-center">
							<div class="category-boxes-item">
								<figure>
									<img src="https://i0.wp.com/www.romadecade.org/wp-content/uploads/2019/03/resep-masakan-ayam.jpg?w=700&ssl=1">
									<h4>Ayam</h4>
									<figcaption><a href="<?php echo base_url();?>menu/?menuType=2" class="btn btn-default-white"><i class="fa fa-shopping-cart"></i> Pesan Sekarang</a>
									</figcaption>
								</figure>
							</div>
						</div>

						<div class="col-md-3 col-sm-6 col-xs-12 text-center">
							<div class="category-boxes-item">
								<figure>
									<img src="http://www.dapurkobe.co.id/wp-content/uploads/bandeng-masak-tauco.jpg">
									<h4>Bandeng</h4>
									<figcaption> <a href="<?php echo base_url();?>menu/?menuType=4" class="btn btn-default-white"><i class="fa fa-shopping-cart"></i> Pesan Sekarang</a> 
									</figcaption>
								</figure>
							</div>
						</div>

						<div class="col-md-3 col-sm-6 col-xs-12 text-center">
							<div class="category-boxes-item">
								<figure>
									<img src="http://www.inijie.com/wp-content/uploads/2016/12/Blue-Band-Pemenang-Master-Oleh-Oleh-005.jpg">
									<h4>Oleh Oleh</h4>
									<figcaption> <a href="<?php echo base_url();?>menu/?menuType=5" class="btn btn-default-white"><i class="fa fa-shopping-cart"></i> Pesan Sekarang</a> 
									</figcaption>
								</figure>
							</div>
						</div>

						<div class="col-md-3 col-sm-6 col-xs-12 text-center">
							<div class="category-boxes-item">
								<figure>
									<img class="img-responsive" src="http://usaharumahan.id/wp-content/uploads/2019/03/resep-minuman-segar-praktis-1024x569.jpg">
									<h4>Minuman</h4>
									<figcaption> <a href="<?php echo base_url();?>menu/?menuType=3" class="btn btn-default-white"><i class="fa fa-shopping-cart"></i> Pesan Sekarang</a> 
									</figcaption>
								</figure>
							</div>
						</div>
						
					</div>
					<!-- end .row -->
				</div>
				<!-- end .category-boxes-icons -->
			</div>
			<!-- container -->

            <!-- star.favorite-menu -->
            <!--
			<div class="favorite-menu">
				<div class="container">
					<h1>Pesanan Terfavorit</h1>
					<p>Konten Artikel Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam.</p>
					<a href="index.html#" class="btn btn-default-red"><i class="fa fa-file-text-o"></i> Read  More</a>
				</div>
            </div>
            -->
            <!-- end .container -->
			<!--end .favorite-menu-->