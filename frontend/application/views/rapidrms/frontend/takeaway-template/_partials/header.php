<header id="header">

			<div class="header-nav-bar">
				<nav class="navbar navbar-default" role="navigation">
					<div class="container">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand" href="<?php echo base_url();?>">
								<img style="margin-top: 5px;" src="<?php echo base_url();?>assets/rapidrms/img/logo-light-krawu.png" alt="<?php echo SITE_NAME;?>">
							</a>
						</div>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav navbar-right">
								<li><a href="<?php echo base_url();?>">Beranda</a>
								</li>
								<li>
									<a href="<?php echo base_url();?>Reservation">Reservasi</a>
								</li>
								<li>
									<a href="<?php echo base_url();?>Menu">Menu</a>
								</li>
								<!--
								<li class="dropdown">
									<a href="index.html#" class="dropdown-toggle" data-toggle="dropdown">News & Events <span class="caret"></span></a>
									<ul class="dropdown-menu" role="menu">
										<li><a href="news-events.html">News & Events list</a></li>
										<li><a href="event-page.html">News & Events post</a></li> 
									</ul>
								</li>
								-->
								<li>
									<a href="<?php echo base_url();?>article">Promo</a>
								</li>
								<!--<li><a href="contact-us.html">Tentang Kami</a>
								</li>-->
								<?php if($this->session->userdata('logged_in')):?>
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown">Akun Saya<span class="caret"></span></a>
										<ul class="dropdown-menu" role="menu">
											<li><a href="<?php echo base_url();?>customer/">Profil</a></li>
											<li><a href="<?php echo base_url();?>auth/logout" onclick="signOut();return false;" >Logout</a></li> 
										</ul>
									</li>
								<?php else:?>
										<li><a href="<?php echo base_url();?>auth">Masuk-Daftar</a>
									</li>
								<?php endif;?>
							</ul>
						</div>
						<!-- /.navbar-collapse -->
					</div>
					<!-- /.container-fluid -->
				</nav>
			</div>
        </header>